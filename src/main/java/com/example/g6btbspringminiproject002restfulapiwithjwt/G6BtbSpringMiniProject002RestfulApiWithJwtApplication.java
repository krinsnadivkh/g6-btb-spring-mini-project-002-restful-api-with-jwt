package com.example.g6btbspringminiproject002restfulapiwithjwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class G6BtbSpringMiniProject002RestfulApiWithJwtApplication {

    public static void main(String[] args) {
        SpringApplication.run(G6BtbSpringMiniProject002RestfulApiWithJwtApplication.class, args);
    }

}
