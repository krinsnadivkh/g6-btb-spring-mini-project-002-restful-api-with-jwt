package com.example.g6btbspringminiproject002restfulapiwithjwt.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @GetMapping("/go")
    public String hello(){
        return "Hi";
    }
}
